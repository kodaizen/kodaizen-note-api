const mysql = require('mysql');
const db = require('./config-db');


const con = mysql.createConnection({
    host: db.host,
    user: db.user,
    password: db.password
});

con.connect(function(err) {
    // if (err) throw err;
    // console.log("Connected to "+db.name+" database.");
    con.query("CREATE DATABASE "+db.name, function (err, result) {
        if (err) throw err;
        console.log("Database created");
    });
});