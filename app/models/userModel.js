let connection = require('../config/db');
let passwordHash = require('password-hash');
let moment = require('./../config/moment');

class User {
    constructor(row) {
        this.user = row || {};
    }

    get id() {
        return this.user.id;
    }
    get firstname() {
        console.log('getter firstname');
        return this.user.firstname.replace(/\b\w/g, l => l.toUpperCase());
    }
    get lastname() {
        return this.user.lastname.replace(/\b\w/g, l => l.toUpperCase());
    }
    get email() {
        return this.user.email;
    }
    get password() {
        return this.user.password;
    }
    get admin() {
        return this.user.admin;
    }
    get active() {
        return this.user.active;
    }
    get secretkey() {
        return this.user.secretkey;
    }
    get created_at() {
        return moment(this.user.created_at).format('DD MM YYYY');
    }

    static create(payload) {
        return new Promise((resolve, reject) => {
            let user = {};
            user.firstname = payload.firstname;
            user.lastname = payload.lastname;
            user.email = payload.email;
            user.password = passwordHash.generate(payload.plainPassword);
            connection.query('INSERT INTO users SET firstname = ?, lastname = ?, email = ?, password = ?, secretkey = ?, created_at = ?',
                [user.firstname, user.lastname, user.email, user.password, '', new Date()], (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(user);
                    }
                });
        });
    }

    static getOneByEmail(email) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT * FROM users WHERE email = ? LIMIT 1', [email], (err, rows) => {
                if (err) {
                    reject(err);
                }
                else {
                    rows.map((item) => new User(item));
                    resolve(rows[0]);
                }
            });
        });
    }

    static getOneById(id) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT * FROM users WHERE id = ? LIMIT 1', [id], (err, rows) => {
                if (err) {
                    reject(err);
                }
                else {
                    rows.map((item) => {
                        return new User(item);
                    });
                    resolve(rows[0]);
                }
            });
        });
    }

    static getAll() {
        return new Promise((resolve, reject) => {
            connection.query('SELECT * FROM users', (err, rows) => {
                if (err) {
                    reject(err);
                }
                else {
                    rows.map((item) => {
                        return new User(item);
                    });
                    resolve(rows);
                }

            });
        });
    }

    static deleteOne(id) {
        return new Promise((resolve, reject) => {
            connection.query('DELETE FROM users WHERE id = ?', [id], (err, rows) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(true);
                }
            });
        });
    }


}

module.exports = User;