let connection = require('../config/db');
let jwt = require('jsonwebtoken');
let jwtconfig = require('../config/jwt-config');

class Token {

    constructor(user) {
        this.token = {};
        this.token.user_id = user.id;
        this.token.data = jwt.sign(user, jwtconfig.secret);
        this.token.created_at = new Date();
    }

    get user_id() {
        return this.token.user_id;
    }

    get data() {
        return this.token.data;
    }

    get created_at() {
        return this.token.created_at;
    }

    static createToken(user) {
        let token = new Token(user);
        return new Promise((resolve, reject) => {
            if (token) {
                connection.query('INSERT INTO tokens SET user_id = ?, value = ?, created_at = ?',
                    [token.user_id, token.data, token.created_at], (err, rows) => {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(token);
                        }
                    });
            }
            else {
                reject('erreur de création du token');
            }
        });
    }

    static deleteOldTokens(current_user) {
        connection.query('DELETE FROM tokens WHERE user_id = ?',[current_user.id], (err, rows) => {
         return err;
            });
    }

}

module.exports = Token;