let mysql = require('mysql');
let db = require('./config-db');

let connection = mysql.createConnection({
    host     : db.host,
    user     : db.user,
    password : db.password,
    database : db.name
});

connection.connect();

module.exports = connection;