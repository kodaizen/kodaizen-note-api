let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let morgan = require('morgan');
let passwordHash = require('password-hash');

let jwt = require('jsonwebtoken');
let jwtconfig = require('./app/config/jwt-config');
let User = require('./app/models/userModel');
let Token = require('./app/models/tokenModel');


//app config------------------------------------------------------------------------------------------------------------
let port = process.env.PORT || 8080;
app.set('superSecret', jwtconfig.secret);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev')); //log url request

//routes----------------------------------------------------------------------------------------------------------------

//create user
app.post('/api/users', function (req, res) {
    User.getOneByEmail(req.body.email)
        .then((user) => {
            if (user !== undefined) {
                return Promise.reject({
                    status_code: 400,
                    message: "Votre compte existe déjà."
                });
            }
        })
        .then(() => User.create(req.body))
        .then((user) => {
            res.status(201).json(user);
        })
        .catch((err) => {
            res.status(err.status_code || 500).json({message: err.message || "Erreur serveur " + err})
        });
});

//get user by id
app.get('/api/users/:id', function (req, res) {
    let id = req.params.id;
    User.getOneById(id)
        .then((user) => {
            if (user === undefined) {
                return Promise.reject({status_code: 404, message: "Utilisateur introuvable."});
            }
            else {
                res.json(user);
            }
        })
        .catch((err) => {
            res.status(err.status_code || 500).json({message: err.message || "Erreur serveur " + err});
        });
});

//get users
app.get('/api/users', function (req, res) {
    User.getAll()
        .then((users) => {
            res.json(users)
        })
        .catch((err) => {
            res.status(err.status_code || 500).json({message: err.message || "Erreur serveur " + err})
        })
});

//update user
app.put('/api/users/:id', function (req, res) {

});

//delete user
app.delete('/api/users/:id', function (req, res) {
    let id = req.params.id;
    User.deleteOne(id)
        .then(() => {
            res.status(204).end();
        })
        .catch((err) => {
            res.status(err.status_code || 500).json({message: err.message || "Erreur serveur " + err});
        })
});

//authenticate user
app.post('/api/authenticate', function (req, res) {
    User.getOneByEmail(req.body.email)
        .then((user) => {
            if (user) {
                if (!passwordHash.verify(req.body.plainPassword, user.password)) {
                    return Promise.reject({
                        status_code: 400,
                        message: "Mot de passe incorrect."
                    })
                }
            }
            else {
                return Promise.reject({
                    status_code: 404,
                    message: "Adresse email incorrecte."
                })
            }
            Token.deleteOldTokens(user);
            return user;
        })
        .then((user) => Token.createToken(user))
        .then((token) => {
            res.status(201).json(token);
        })
        .catch((err) => {
            res.status(err.status_code || 500).json({message: err.message || "Erreur serveur " + err});
        })
});


//start server----------------------------------------------------------------------------------------------------------
app.listen(port, function () {
    console.log('app listening on port ' + port);
});